/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class EmployeeCatalog {
    private ArrayList<Salesperson> ec;

    public EmployeeCatalog() {
        ec = new ArrayList<Salesperson>();
    }

    public ArrayList<Salesperson> getEc() {
        return ec;
    }
    
    public Salesperson addSalesperson()
    {
        Salesperson sp = new Salesperson();
        ec.add(sp);
        return sp;
    }
    
    public void deleteSalesperson(Salesperson sp)
    {
        ec.remove(sp);
    }
    
    public Salesperson findSalesperson(String name)
    {
        for (Salesperson sp : ec)
            if (name.equals(sp.getFname()+" "+sp.getLname()))
                return sp;
        return null;
    }
    
    public Salesperson findbyid(int id){
        for (Salesperson sp : ec)
            if (sp.getId() == id)
                return sp;
        return null;
    }
            
}
