/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class Supplier {
    private String name;
    private ProductCatalog pc;
    private Account account;
    private int id;
    private static int suppliercount = 0;

    public Supplier() {
        pc = new ProductCatalog();
        account = null;
        suppliercount++;
        id = suppliercount;
    }
    
    @Override
    public String toString()
    {
        return name;
    }

    public int getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductCatalog getPc() {
        return pc;
    }

    public void setPc(ProductCatalog pc) {
        this.pc = pc;
    }

    public static int getSuppliercount() {
        return suppliercount;
    }
    
 

    
    
    
}
