/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Reyes
 */
public class Product {
    private String name;
    private static int productCount = 0;
    private int id;
    private double cost;
    private MarketOffer mo;

    public Product() {
        productCount++;
        this.id = productCount;
        mo = new MarketOffer();
    }

    public MarketOffer getMo() {
        return mo;
    }

    public void setMo(MarketOffer mo) {
        this.mo = mo;
    }    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getProductCount() {
        return productCount;
    }

    public static void setProductCount(int productCount) {
        Product.productCount = productCount;
    }

    public int getId() {
        return id;
    }


    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    
    @Override
    public String toString()
    {
        return name;
    }

}
