/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class OrderCatalog {
    private ArrayList<Order> oc;

    public OrderCatalog() {
        oc = new ArrayList<Order>();
    }

    public ArrayList<Order> getOc() {
        return oc;
    }
    
    public Order addOrder()
    {
        Order o = new Order();
        oc.add(o);
        return o;
    }
    
    public void addexistorder(Order o)
    {
        oc.add(o);
    }
    
    public void deleteOrder(Order o)
    {
        oc.remove(o);
    }
    
    public Order findOrder (int id)
    {
        for (Order o : oc)
            if (o.getOrderid()==id)
                return o;
        return null;
    }
}
