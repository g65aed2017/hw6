/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.ArrayList;

/**
 *
 * @author Reyes
 */
public class MarketOfferCatalog {
    private ArrayList<MarketOffer> moc;

    public MarketOfferCatalog() {
        moc = new ArrayList<MarketOffer>();
    }

    public ArrayList<MarketOffer> getMoc() {
        return moc;
    }

    public void setMoc(ArrayList<MarketOffer> moc) {
        this.moc = moc;
    }
    
    
    public MarketOffer addMarketOffer()
    {
        MarketOffer mo = new MarketOffer();
        moc.add(mo);
        return mo;        
    }
    
    public void deleteMarketOffer(MarketOffer mo)
    {
        moc.remove(mo);
    }
    
    public MarketOffer getMarketOffer(int i)
    {
        return moc.get(i);
    }

}
