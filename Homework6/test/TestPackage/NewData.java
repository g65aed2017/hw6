/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestPackage;

import java.io.*;
import java.util.Random;

/**
 *
 * @author Reyes
 */
public class NewData {
   
    public static double round(double value)
    {
        int f = (int) Math.pow(10, 2);
        value *=f;
        double v = Math.round(value);
        return v/f;        
    }
    
    public static void main(String[] args) {
        
        
        try {
            File file = new File("HW6.txt");
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            Random r = new Random();
            
            int suppliernum = 10;
            int productnum = 0;
            double[][] price = new double[100][2];
            bw.write(suppliernum+"\r\n");
            for (int i = 1; i <= suppliernum; i++)
            {
                int pronum = r.nextInt(10)+1;
                
                bw.write("Supplier"+i+","+pronum+"\r\n");
                for (int j = 1; j<=pronum; j++)
                {
                    int cost = (r.nextInt(100)+50)*10;
                    price[j+productnum][0] = cost - ((r.nextInt(45)+5)*10); //floor
                    price[j+productnum][1] = cost + (r.nextInt(200)*10);    //ceiling
                    //int target = r.nextInt(ceiling - floor) + floor;
                    bw.write("Product"+(j+productnum)+","+cost+","+price[j+productnum][0]+","+price[j+productnum][1]+"\r\n");
                }     
                productnum += pronum;
            }
           
            int marketnum = 7;
            int customernum = 0;
            bw.write(marketnum+"\r\n");
            for (int i = 0; i < marketnum; i++)
            {
                int cus = r.nextInt(5)+1;
                bw.write("Market"+(i)+","+cus+"\r\n");
                for (int j = 0; j < cus; j++)
                {
                    bw.write("Customer"+(j+customernum)+",");
                }
                customernum += cus;
                bw.write("\r\n");
                for (int j = 1; j <= productnum; j++)
                {
                    double floor = round(price[j][0] + (price[j][1]-price[j][0])/marketnum*i);
                    double ceiling = round(price[j][0] + (price[j][1]-price[j][0])/marketnum*(i+1));
                    double target = round(floor + (ceiling - floor) * r.nextDouble());
                    bw.write(floor+","+ceiling+","+target+"\r\n");
                }                
            }                      
            
            
            
            bw.flush();  
            bw.close();    
        } catch (Exception e) {
            e.printStackTrace(); 
        }
        
        
    }
}
